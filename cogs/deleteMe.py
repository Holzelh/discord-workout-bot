from discord.ext import commands
import sqlite3

import cogs.util.dbFuncs as sql_db

class deleteMe(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def deleteMe(self, ctx):
        """
        This command deletes the user from the database,
        this only deletes the person who runs the command,
        discord id is taken in order to find respective data
        and delete it
        """

        await ctx.send("Deleting your data..")
        sql_db.delete_user(ctx.author.id)

        return

def setup(bot):
    bot.add_cog(deleteMe(bot))
