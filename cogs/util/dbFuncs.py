# all functions to handle sqlite queries

import sqlite3

def sql_creator(element):
    """
    Returns a column depending
    on the number passed in. This is
    used in order to pick which column
    to execute a query on.

    :param element: the number of a column in the db
    :type element: int
    :return: The name of the respective column
    :rtype: string
    """
    db_column = ('id', 'name', 'streak', 'weight', 'calLimit', 'worked', 'dailyCal',)
    return db_column[element]

def create_db():
    """
    Creates the table if it doesn't
    already exist.
    Sets the discord id as the
    primary key
    """
    conn = sqlite3.connect('examp.db')
    c = conn.cursor()

    #create table examp
    c.execute('''CREATE TABLE IF NOT EXISTS examp (
    id integer PRIMARY KEY,
    name text,
    streak integer,
    weight integer,
    calLimit integer,
    worked integer,
    dailyCal integer
    )''')

    conn.commit()
    conn.close()
    return

def create_connection():
    """
    Returns connection
    to the db. Used in
    other sql functions to
    shorten the code.
    """
    conn = sqlite3.connect('examp.db')
    return conn


def add_new_user(user_id, user_name):
    """
    This function adds a new user
    to the db and sets only
    their id and name, the
    rest of the values are 0.

    :param user_id: The user's discord id, used as primary key in db.
    :type user_id: int
    :param user_name: The user's discord username + tag. EX: Holzel#5555
    :type user_name: string
    """
    conn = create_connection()
    c = conn.cursor()
    user = [(str(user_id),str(user_name),'0','0','0','0','0',),]
    try:
        with conn:
            c.executemany('INSERT INTO examp VALUES (?,?,?,?,?,?,?)', user)
    except sqlite3.IntegrityError as e:
        print(e)

    conn.close()

def delete_user(user_id):
    """
    This function deletes
    the user from the db.

    :param user_id: The user's discord id, used as primary key in the db.
    :type user_id: int
    """
    conn = create_connection()
    c = conn.cursor()
    user = (str(user_id),)
    try:
        with conn:
            c.execute('DELETE FROM examp WHERE rowid=?', user)
    except sqlite3.IntegrityError as e:
        print(e)
    else:
        print("Success deleting user!")
    conn.close()

def update_value(user_id, user_val,col):
    """
    Update a value in the
    db for the user
    that corresponds
    to the id

    :param user_id: The user's discord id, used as primary key in the db.
    :type user_id: int

    :param user_val: The value passed in by the user which is to be placed in a column.
    :type user_val: int

    :param col: Which column in db to use (see sql_creator())
    :type col: int
    """
    conn = create_connection()
    c = conn.cursor()
    user = (str(user_val), str(user_id),)
    sql = '''UPDATE examp SET ''' + sql_creator(col) + ''' =? ''' + '''WHERE rowid=?'''
    try:
        with conn:
            c.execute(sql,user)
    except sqlite3.IntegrityError as e:
        print(e)
    else:
        print("Success updating value!")
    conn.close()

def get_value(user_id, col):
    """
    Retrieve value from the db
    for the user.

    :param user_id: User's discord id, id serves as primary key
    :type user_id: int

    :param col: Which column in db to use (see sql_creator())
    :type col: int
    """
    conn = create_connection()
    c = conn.cursor()
    user = (str(user_id),)
    sql = '''SELECT ''' + sql_creator(col) + ''' FROM examp WHERE rowid=?'''
    try:
        with conn:
            c.execute(sql, user)
            value = c.fetchone()
            return value[0]
    except sqlite3.IntegrityError as e:
        print(e)
    else:
        print("Success!")

def just_debug():
    """
    Print everything
    inside the db, no formatting.
    """
    conn = create_connection()
    c = conn.cursor()
    try:
        with conn:
            c.execute('''SELECT * FROM examp''')
            x = c.fetchall()
            print(x)
    except sqlite3.IntegrityError as e:
        print(e)
