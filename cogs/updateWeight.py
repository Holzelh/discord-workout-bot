from discord.ext import commands
import sqlite3

import cogs.util.dbFuncs as sql_db

class updateWeight(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def updateWeight(self, ctx, userW):
        """
        This command updates the user's weight,
        tells the user if they gained/lost weight,
        and by how much they gained/lost.

        :param userW: Weight the user entered
        :type userW: str
        """

        await ctx.send("Updating your current weight..")

        prev_weight = sql_db.get_value(ctx.author.id, 3)
        if int(userW) > prev_weight:
            weight_gained = int(userW) - prev_weight
            msg = f"You have gained {weight_gained} lbs."
            await ctx.send(msg)
        elif int(userW) < prev_weight:
            weight_lost = prev_weight - int(userW)
            msg = f"You have lost {weight_lost} lbs!"
            await ctx.send(msg)
        else:
            msg = "You have not gained or lost weight!"
            await ctx.send(msg)

        sql_db.update_value(ctx.author.id, userW, 3)

        return

def setup(bot):
    bot.add_cog(updateWeight(bot))
