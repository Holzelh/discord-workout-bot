from discord.ext import commands
import cogs.util.dbFuncs as sql_db

class checkin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def checkin(self, ctx):
        """
        This command updates the user's streak,
        increases current value by 1,
        this command can only be run once
        a day by the respective user
        """

        workedOut = sql_db.get_value(ctx.author.id, 5)
        if workedOut == 0:
            await ctx.send("Updated your streak!")
            userStreak = sql_db.get_value(ctx.author.id, 2)
            print(str(workedOut) + " THIS")
            sql_db.update_value(ctx.author.id, (int(userStreak) + 1), 2)
        else:
            await ctx.send("You already checked in today!")
        return

def setup(bot):
    bot.add_cog(checkin(bot))
