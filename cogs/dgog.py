from discord.ext import commands
import sqlite3

import cogs.util.dbFuncs as sql_db

class dgog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def dgog(self, ctx):
        """
        This command is simply a debug command,
        it will print the contents of the entire
        database onto the console
        """

        await ctx.send("Debugging..")
        sql_db.just_debug()


        return

def setup(bot):
    bot.add_cog(dgog(bot))
