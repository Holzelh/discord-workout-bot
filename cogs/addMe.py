from discord.ext import commands
import sqlite3

import cogs.util.dbFuncs as sql_db


class addMe(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def addMe(self, ctx):
        """
        This command adds the user to the database
        by automatically taking their discord id and username,
        then filling the remaining user respective rows with 0's
        """

        await ctx.send("Adding new user")
        sql_db.add_new_user(str(ctx.author.id),str(ctx.author))
        sql_db.just_debug()

def setup(bot):
    bot.add_cog(addMe(bot))
