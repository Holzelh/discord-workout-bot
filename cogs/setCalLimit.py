from discord.ext import commands
import sqlite3

import cogs.util.dbFuncs as sql_db

class setCalLimit(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def setCalLimit(self, ctx, userCal):
        """
        This command updates the user's calorie
        limit, it automatically takes the user id to
        use as the primary key
        """

        await ctx.send("Updating your current calorie limit..")
        sql_db.update_value(ctx.author.id, userCal, 4)

        return

def setup(bot):
    bot.add_cog(setCalLimit(bot))
