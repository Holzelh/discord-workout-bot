# Discord Workout Bot

A Discord workout bot written in python. 

# Features (More to come + Ideas are welcome!)

* Tells you how many calories you have left depending on your limit
* Saves your calorie limit
* Saves your weight and tells you if you've gained or lost any weight
* Allows for Workout streaks, compete with friends!

# Required packages (Linux)

* python 3.5.3 or higher
* [discord.py](https://discordpy.readthedocs.io/en/latest/intro.html)
* sqlite3

# Installation (Linux)

``` sh
git clone https://gitlab.com/Holzelh/discord-workout-bot.git
```
* cd discord-workout-bot
* create bot_key.py
* store bot token inside like so:

``` python
bot_token = "YourTokenGoesInHere"
```
* run bot.py

``` sh
python3 bot.py
```
# Commands + Usage

Command | Arguments | What it does | Usage Example
------- | --------- | ------------ | -------------
.addMe | None | Adds the user to the database(users need to run this **First**) | .addMe
.deleteMe | None | Deletes the user from the database(they will need to run .addMe again) | .deleteMe
.setCalLimit | Integer | Sets the daily calorie limit for the user | .setCalLimit 1500
.updateWeight | Integer | stores your current weight, will tell user if they've gained or lost any weight since the last time they entered a weight | .updateWeight 150
.dgog | None | This is just a debug command, it prints out the entire database onto the console(**not visible in discord**) | .dgog
