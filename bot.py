
import bot_key
import discord
from discord.ext import commands

bot = commands.Bot(command_prefix='.')

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    await bot.change_presence(status=discord.Status.online, activity=discord.Game("TEST"))

# load cogs
bot.load_extension("cogs.addMe")
bot.load_extension("cogs.deleteMe")
bot.load_extension("cogs.updateWeight")
bot.load_extension("cogs.dgog")
bot.load_extension("cogs.setCalLimit")
bot.load_extension("cogs.checkin")

bot.run(bot_key.bot_token)
